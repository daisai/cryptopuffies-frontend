import React from 'react'
import styled from 'styled-components'
import { Button, useWalletModal } from 'penguinfinance-uikit2'
import useAuth from 'hooks/useAuth'

const UnlockButton = (props) => {
  const { login, logout } = useAuth()
  const { onPresentConnectModal } = useWalletModal(login, logout)

  return (
    <StyledUnlockedButton onClick={onPresentConnectModal} {...props}>
      Connect Wallet
    </StyledUnlockedButton>
  )
}

const StyledUnlockedButton = styled(Button)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  color: #9d7eff;
  padding: 15px 20px;
  background: white;
  box-shadow: none;
  border: 1px solid #9d7eff;
  box-sizing: border-box;
  border-radius: 32px;

  @media (min-width: 768px) {
    height: 48px;
  }

  @media (min-width: 1200px) {
    height: 56px;
  }
`

export default UnlockButton

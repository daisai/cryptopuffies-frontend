const config = {
  menuEntries: [
    {
      label: 'Cryptopuffies',
      value: 'cryptopuffies',
      href: '/',
    },
    {
      label: 'About',
      value: 'about',
      href: '/',
    },
    {
      label: 'Roadmap',
      value: 'roadmap',
      href: '/',
    },
    {
      label: 'Team',
      value: 'team',

      href: '/',
    },
    {
      label: 'Explore Collection',
      value: 'explore',
      href: '/',
    },
    {
      label: 'FAQs',
      value: 'faqs',
      href: '/',
    },
  ],

  socialLinks: [
    {
      name: 'twitter',
      value: 'twitter',
      icon: 'twitter.svg',
      href: '/',
    },
    {
      name: 'discord',
      value: 'discord',
      icon: 'discord.svg',
      href: '/',
    },
    {
      name: 'instagram',
      value: 'instagram',
      icon: 'instagram.svg',
      href: '/',
    },
  ],
}
export default config

import React, { useState } from 'react'
import styled from 'styled-components'
import { Flex, ResetCSS } from 'penguinfinance-uikit2'
import SvgIcon from 'components/SvgIcon'
import UnlockButton from 'components/UnlockButton'
import useWindowSize from 'hooks/useWindowSize';
import config from './config'

const Header = () => {
  const windowSize = useWindowSize();
  const [activeSection, setActiveSection] = useState('cryptopuffies')
  const [viewMenu, setViewMenu] = useState(false)
  const { menuEntries, socialLinks } = config

  const onClickMenuItem = (value: string) => {
    setActiveSection(value)

    if (viewMenu) {
      setViewMenu(false);
    }
  }

  const handleOpenMenu = () => {
    setViewMenu(true);
  }

  const handleCloseMenu = () => {
    setViewMenu(false);
  }

  return (
    <>
      <ResetCSS />
      <HeaderContainer>
        {windowSize.width < 1200 ? 
          <Flex alignItems="center" justifyContent="space-between" width="100%">
            {windowSize.width < 768 ?
              <>
                <Menu>
                  {[menuEntries[0]].map((row) => {
                    return (
                      <MenuItem
                        key={row.value}
                        isActive={activeSection === row.value}
                        onClick={() => onClickMenuItem(row.value)}
                      >
                        {row.label}
                      </MenuItem>
                    )
                  })}
                </Menu>
                <Flex onClick={handleOpenMenu} >
                  <SvgIcon src={`${process.env.PUBLIC_URL}/images/header/hamburger-right.svg`} width="24px" height="24px" />
                </Flex>
              </>
            :
              <>
                <Flex onClick={handleOpenMenu}>
                  <SvgIcon src={`${process.env.PUBLIC_URL}/images/header/hamburger-left.svg`} width="24px" height="24px" />
                </Flex>
                <Menu>
                  {[menuEntries[0]].map((row) => {
                    return (
                      <MenuItem
                        key={row.value}
                        isActive={activeSection === row.value}
                        onClick={() => onClickMenuItem(row.value)}
                      >
                        {row.label}
                      </MenuItem>
                    )
                  })}
                </Menu>
                <UnlockButton />
              </>
            }
          </Flex>
          : <Flex alignItems="center" justifyContent="space-between" width="100%">
            <Menu>
              {menuEntries.map((row) => {
                return (
                  <MenuItem
                    key={row.value}
                    isActive={activeSection === row.value}
                    onClick={() => onClickMenuItem(row.value)}
                  >
                    {row.label}
                  </MenuItem>
                )
              })}
            </Menu>
            <SocialLinksContainer>
              {socialLinks.map((row) => {
                return (
                  <SocialLinkItem key={row.value} onClick={() => onClickMenuItem(row.value)}>
                    <SvgIcon src={`${process.env.PUBLIC_URL}/images/header/${row.icon}`} width="24px" height="24px" />
                  </SocialLinkItem>
                )
              })}
            </SocialLinksContainer>
            <WalletActionContainer>
              <UnlockButton />
            </WalletActionContainer>
          </Flex>
        }
      </HeaderContainer>
      <MenuPanel isPushed={viewMenu}>
        {viewMenu && 
          <>
            <MenuHeader padding='16px' justifyContent='space-between'>
              <Menu>
                {[menuEntries[0]].map((row) => {
                  return (
                    <MenuItem
                      key={row.value}
                      isActive={activeSection === row.value}
                      onClick={() => onClickMenuItem(row.value)}
                    >
                      {row.label}
                    </MenuItem>
                  )
                })}
              </Menu>
              <Flex onClick={handleCloseMenu} >
                <SvgIcon src={`${process.env.PUBLIC_URL}/images/header/hamburger-close.svg`} width="24px" height="24px" />
              </Flex>
            </MenuHeader>
            <Flex flexDirection='column' alignItems='center'>
              <Menu flexDirection='column' alignItems='center' mb='40px'>
                {menuEntries.filter((row, index) => index >= 1).map((row) => {
                  return (
                    <MenuItem
                      key={row.value}
                      isActive={activeSection === row.value}
                      onClick={() => onClickMenuItem(row.value)}
                    >
                      {row.label}
                    </MenuItem>
                  )
                })}
              </Menu>
              <UnlockButton />
              <SocialLinksContainer mt='40px'>
                {socialLinks.map((row) => {
                  return (
                    <SocialLinkItem key={row.value} onClick={() => onClickMenuItem(row.value)}>
                      <SvgIcon src={`${process.env.PUBLIC_URL}/images/header/${row.icon}`} width="24px" height="24px" />
                    </SocialLinkItem>
                  )
                })}
              </SocialLinksContainer>
            </Flex>
          </>
        }
      </MenuPanel>
    </>
  )
}

const HeaderContainer = styled(Flex)`
  width: '100%';
  height: 56px;
  padding: 0px 16px;
  max-width: 1440px;
  margin: auto;
  align-items: center;

  @media (min-width: 768px) {
    height: 87px;
    padding: 0px 40px;
  }
  @media (min-width: 1200px) {
    height: 105px;
  }
  @media (min-width: 1440px) {
    padding: 0px 105px 0px 105px;
  }

  & svg {
    cursor: pointer;
  }
`

// menu
const Menu = styled(Flex)``
const MenuItem = styled.div<{ isActive?: boolean }>`
  font-family: ${({ isActive }) => (isActive ? 'Gotham-bold' : 'Gotham')};
  font-style: normal;
  font-weight: normal;
  font-weight: ${({ isActive }) => (isActive ? 'bold' : 'normal')};
  font-size: 18px;
  line-height: 27px;
  margin-right: 0;
  cursor: pointer;
  
  margin-top: 32px;

  &:first-child {
    font-size: 20px;
    margin-top: 0;
  }

  &:last-child {
    margin-right: 0px;
  }

  @media (min-width: 1200px) {
    margin-right: 40px;
    margin-top: 0;

    &:first-child {
      margin-top: 0;
    }
  }
`
// social links
const SocialLinksContainer = styled(Flex)`
  // margin-left: 90px;

  @media (min-width: 1200px) {
    margin-left: 90px;
  }
`
const SocialLinkItem = styled.div<{ isActive?: boolean }>`
  margin-right: 40px;
  cursor: pointer;

  &:last-child {
    margin-right: 0;
  }
`

// wallet actions
const WalletActionContainer = styled(Flex)``

const MenuPanel = styled.div<{ isPushed: boolean }>`
  position: fixed;
  background: white;
  width: ${({ isPushed }) => (isPushed ? `100%` : 0)};
  height: 100vh;
  z-index: 20;
  transition: width 0.2s;
  left: 0;
  top: 0;
  
  svg {
    cursor: pointer;
  }
`;

const MenuHeader = styled(Flex)`
  padding: 16px;

  @media (min-width: 768px) {
    padding: 32px 40px;
  }
`;

export default Header

import React, { Suspense, lazy, useEffect } from 'react'
import { Router, Route, Switch, useLocation } from 'react-router-dom'
import BigNumber from 'bignumber.js'
import { useFetchPublicData } from 'state/hooks'
import GlobalStyle from 'style/Global'
import PageLoader from 'components/PageLoader'
import CurrentBlockWrapper from 'components/CurrentBlockWrapper'
import Header from 'components/Header'
import Footer from 'components/Footer/Footer'
import usePersistConnect from 'hooks/usePersistConnect'
import history from './routerHistory'

const CryptoPuffies = lazy(() => import('./views/CryptoPuffies'))
const NotFound = lazy(() => import('./views/NotFound'))

// This config is required for number formatting
BigNumber.config({
  EXPONENTIAL_AT: 1000,
  DECIMAL_PLACES: 80,
})

const ScrollToTop = () => {
  const { pathname } = useLocation()

  useEffect(() => {
    document.getElementById('root').scrollTo(0, 0)
  }, [pathname])

  return null
}

const App: React.FC = () => {
  useFetchPublicData()
  usePersistConnect()

  return (
    <Router history={history}>
      <GlobalStyle />
      <ScrollToTop />
      <Suspense fallback={<PageLoader />}>
        <Header />
        <Switch>
          <Route path="/" exact>
            <CryptoPuffies />
          </Route>
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </Suspense>
      <CurrentBlockWrapper />
    </Router>
  )
}

export default React.memo(App)

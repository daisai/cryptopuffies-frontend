import React from 'react'
import styled from 'styled-components'
import { Flex } from 'penguinfinance-uikit2'
import Page from 'components/layout/Page'
import TopBanner from './components/TopBanner'
import MarvelousNFT from './components/MarvelousNFT'
import WhatCryptopuffies from './components/WhatCryptopuffies'
import Welcome from './components/Welcome'
import HowToWork from './components/HowToWork'
import Roadmap from './components/Roadmap'
import CommunityTools from './components/CommunityTools'
import IntroducingTeam from './components/IntroducingTeam'
import ExploreCollection from './components/ExploreCollection'
import Faq from './components/Faq'

const CryptoPuffies: React.FC = () => {
  return (
    <>
      <StyledPage>
        <TopBanner />
        <MarvelousNFTSection>
          <BluePuffyImgWrapper>
            <BluePuffyImg
              src={`${process.env.PUBLIC_URL}/images/cryptopuffies/marvelous/blue_puffy.svg`}
              alt="blue puffy"
            />
          </BluePuffyImgWrapper>
          <MarvelousNFT />
          <YellowPuffyImgWrapper>
            <YellowPuffyImg
              src={`${process.env.PUBLIC_URL}/images/cryptopuffies/marvelous/yellow_puffy.svg`}
              alt="yellow puffy"
            />
          </YellowPuffyImgWrapper>
        </MarvelousNFTSection>
        <AContainer>
          <WhatCryptopuffiesSection>
            <WhatCryptopuffies />
          </WhatCryptopuffiesSection>
          <WelcomeSection>
            <Welcome />
          </WelcomeSection>
          <HowToWorkSection>
            <HowToWork />
          </HowToWorkSection>
        </AContainer>
        <RoadmapSection>
          <Roadmap />
        </RoadmapSection>
        <CommunityToolsSection>
          <CommunityTools />
        </CommunityToolsSection>
        <IntroducingTeamSection>
          <IntroducingTeam />
        </IntroducingTeamSection>
        <ExploreCollectionSection>
          <ExploreCollection />
        </ExploreCollectionSection>
        <FaqSection>
          <Faq />
        </FaqSection>
      </StyledPage>
    </>
  )
}

const StyledPage = styled(Page)`
  max-width: 1440px;
  padding-top: 0px;
  padding: 0px;
`

// marvelous nft section
const MarvelousNFTSection = styled.div`
  margin-top: -40px;

  @media (min-width: 768px) {
    margin-top: -80px;
  }
  @media (min-width: 1200px) {
    margin-top: -130px;
  }
`

const BluePuffyImgWrapper = styled(Flex)``

const BluePuffyImg = styled.img`
  z-index: 1;
  width: 180px;
  max-width: 180px;
  margin-bottom: -90px;
  margin-left: -20px;

  @media (min-width: 768px) {
    max-width: 240px;
    width:240px;
    margin-bottom: -120px;
    margin-left: -20px;
  }
  @media (min-width: 968px) {
    width: 320px;
    max-width: 320px;
    margin-bottom: -180px;
  }
  @media (min-width: 1200px) {
    width: 490px;
    max-width: 490px;
    margin-bottom: -240px;
    margin-left: 30px;
  }
`

const YellowPuffyImgWrapper = styled(Flex)`
  justify-content: flex-end;
  margin-top: -132px;
  margin-right: -60px;

  @media (min-width: 768px) {
    margin-top: -180px;
    margin-right: -60px;
  }
  @media (min-width: 968px) {
    margin-top: -250px;
    margin-right: 30px;
  }
`

const YellowPuffyImg = styled.img`
  z-index: 1;
  width: 170px;
  max-width: auto;

  @media (min-width: 768px) {
    width: 288px;
    max-width: 288px;
    margin-bottom: -180px;
  }
  @media (min-width: 1200px) {
    width: 429px;
    max-width: 429px;
    margin-bottom: 0px;
  }
`

const AContainer = styled.div`
  background: linear-gradient(357.65deg, #f0ebff -38.44%, rgba(255, 255, 255, 0) 98.03%);
  padding-bottom: 48px;
  margin-top: 0px;

  @media (min-width: 768px) {
    margin-top: 200px;
    padding-bottom: 120px;
  }
  @media (min-width: 1200px) {
    margin-top: 0;
  }
`

// what are cryptopuffies section
const WhatCryptopuffiesSection = styled.div`
  margin-top: 0px;
  padding: 0px 16px;

  @media (min-width: 768px) {
    margin-top: 0;
    padding: 0px 28px 0px 40px;
  }
  @media (min-width: 1200px) {
    margin-top: -60px;
    margin-bottom: 120px;
    padding: 0px 32px 0px 105px;
  }
`

// welcome section
const WelcomeSection = styled.div`
  margin-top: 80px;
  padding: 0px 16px;
  @media (min-width: 768px) {
    padding: 0px 32px;
  }
  @media (min-width: 1200px) {
    padding: 0px 40px;
  }
  @media (min-width: 1400px) {
    padding: 0px 105px;
  }
`

// how to work section
const HowToWorkSection = styled.div`
  margin-top: 44px;
  padding: 0 16px;

  @media (min-width: 768px) {
    padding: 0px 40px;
  }
`

// roadmap section
const RoadmapSection = styled.div`
  margin-top: 50px;
  padding: 0px 16px;
  @media (min-width: 768px) {
    padding: 0px 32px;
    margin-top: 120px;
  }
  @media (min-width: 1200px) {
    padding: 0px 40px;
  }
  @media (min-width: 1400px) {
    padding: 0px 105px;
  }
`

// community tools section
const CommunityToolsSection = styled.div`
  margin-top: 120px;
  padding: 0px 16px;
  padding-top: 50px;
  padding-bottom: 100px;
  background: linear-gradient(173.91deg, #f0ebff -24.58%, rgba(240, 235, 255, 0) 81.97%);
  
  @media (min-width: 768px) {
    padding: 0px 32px;
    padding-top: 120px;
    padding-bottom: 40px;
  }
  @media (min-width: 1200px) {
    padding: 0px 40px;
    padding-top: 120px;
    padding-bottom: 207px;
  }
  @media (min-width: 1400px) {
    padding: 0px 105px;
    padding-top: 120px;
  }
`

// introducing team section
const IntroducingTeamSection = styled.div`
  padding: 0px 16px;
  padding-top: 0px;
  padding-bottom: 50px;

  @media (min-width: 768px) {
    padding: 0px 32px;
    padding-top: 82px;
    padding-bottom: 120px;
  }
  @media (min-width: 1200px) {
    padding: 0px 40px;
    padding-top: 82px;
    padding-bottom: 120px;
  }
  @media (min-width: 1400px) {
    padding: 0px 105px;
    padding-top: 82px;
    padding-bottom: 180px;
  }
`

// explore collection
const ExploreCollectionSection = styled.div`
  padding: 0px 16px;

  @media (min-width: 768px) {
    padding: 0px 32px;
    padding-top: 20px;
    padding-bottom: 40px;
  }
  @media (min-width: 1200px) {
    padding: 0px 40px;
    padding-top: 20px;
    padding-bottom: 40px;
  }
  @media (min-width: 1400px) {
    padding: 0px 105px;
    padding-top: 59px;
    padding-bottom: 120px;
  }
`

// faq section
const FaqSection = styled.div`
  padding: 0px 16px;
  padding-top: 100px;
  padding-bottom: 40px;
  background: linear-gradient(6deg, #f0ebff -24.58%, rgba(240, 235, 255, 0) 81.97%);
  
  @media (min-width: 768px) {
    padding: 0px 32px;
    padding-top: 100px;
    padding-bottom: 120px;
  }
  @media (min-width: 1200px) {
    padding: 0px 40px;
    padding-top: 20px;
    padding-bottom: 120px;
  }
  @media (min-width: 1400px) {
    padding: 0px 105px;
    padding-top: 120px;
    padding-bottom: 120px;
  }
`;

export default CryptoPuffies

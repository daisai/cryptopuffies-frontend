import React, { useState } from 'react'
import styled from 'styled-components'
import { Flex, Text } from 'penguinfinance-uikit2'
import SvgIcon from 'components/SvgIcon'

interface FaqCardProps {
  id: number
  question: string
  answer: string
}

const faqs = [
  {
    id: 1,
    question: 'What is an NFT?',
    answer: `You are free to do anything with them under 
  a non-exclusive license.`,
  },
  {
    id: 2,
    question: 'Is Сrypto Puffies a team of people, or just one?',
    answer: `You are free to do anything with them under 
  a non-exclusive license.`,
  },
  {
    id: 3,
    question: 'How do I get on the drop calendar?',
    answer: `You are free to do anything with them under 
  a non-exclusive license.`,
  },
  {
    id: 4,
    question: 'Are Crypto Puffies a good investment?',
    answer: `You are free to do anything with them under 
  a non-exclusive license.`,
  },
  {
    id: 5,
    question: 'What can I do with my Crypto Puffies?',
    answer: `You are free to do anything with them under 
  a non-exclusive license.`,
  },
  {
    id: 6,
    question: 'What is Metamask?',
    answer: `You are free to do anything with them under 
  a non-exclusive license.`,
  },
]

const FaqCard = ({ data }: { data: FaqCardProps }) => {
  const [isExpanded, setExpanded] = useState(false)

  const onToggle = () => {
    setExpanded(!isExpanded)
  }

  return (
    <FaqCardContainer onClick={onToggle}>
      <FaqOrigin>
        <FaqQuestion>{data.question}</FaqQuestion>
        <SvgIcon
          src={`${process.env.PUBLIC_URL}/images/cryptopuffies/faq/${isExpanded ? 'minus_icon.svg' : 'plus_icon.svg'}`}
          width="24px"
          height="24px"
        />
      </FaqOrigin>
      {isExpanded && (
        <FaqExpanded>
          <FaqAnswer>{data.answer}</FaqAnswer>
        </FaqExpanded>
      )}
    </FaqCardContainer>
  )
}

const Faq = () => {
  return (
    <>
      <FaqContainer>
        <FaqHeader>
          <FaqHeaderTitle>
            <FirstSentence>{`Puffies `}</FirstSentence>
            <SecondSentence>{`FAQs `}</SecondSentence>
          </FaqHeaderTitle>
          <FaqHeaderDescription>
            {`If you cannot find answer to your question below, feel free to reach us at hello@cryptopuffies.com `}
          </FaqHeaderDescription>
          <FaqPuffyImgWrapper>
            <FaqPuffyImg src={`${process.env.PUBLIC_URL}/images/cryptopuffies/faq/faq_puffy.png`} alt="faq puffy" />
          </FaqPuffyImgWrapper>
        </FaqHeader>
        <FaqContent>
          {faqs.map((faq) => (
            <FaqCard key={faq.id} data={faq} />
          ))}
        </FaqContent>
      </FaqContainer>
    </>
  )
}

const FaqContainer = styled(Flex)`
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`;

// header
const FaqHeader = styled.div`
  width: 100%;

  @media (min-width: 768px) {
    width: 50%;
    padding-right: 24px;
  }
`
const FaqHeaderTitle = styled(Flex)`
  white-space: break-spaces;
  flex-wrap: wrap;
`
const FirstSentence = styled.span`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`

const SecondSentence = styled.span`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`
const FaqHeaderDescription = styled.div`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 27px;
  padding-right: 60px;
  margin-top: 8px;

  @media (min-width: 768px) {
    font-size: 18px;
  }
`

// sub list
const FaqContent = styled.div`
  width: 100%;

  @media (min-width: 768px) {
    margin-left: 50px;
  }
`

const FaqCardContainer = styled.div`
  padding: 14px 16px;
  width: 100%;
  background: #ffffff;
  box-shadow: 4px 4px 15px rgba(166, 129, 255, 0.08);
  border-radius: 8px;
  margin-bottom: 16px;
  cursor: pointer;

  @media (min-width: 768px) {
    padding: 24px 32px;
  }
`
const FaqOrigin = styled(Flex)`
  justify-content: space-between;
`
const FaqExpanded = styled.div`
  transition: height 1.5s ease;
`

const FaqQuestion = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  color: #050021;
`
const FaqAnswer = styled(Text)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 24px;
  color: #050021;
  max-width: 465px;
  
  @media (min-width: 768px) {
    font-size: 18px;
    line-height: 27px;
  }
`

// faq puffy
const FaqPuffyImgWrapper = styled(Flex)`
  margin-top: 20px;
  
  @media (min-width: 768px) {
    // max-width: 376px;
  }
  @media (min-width: 1200px) {
    margin-left: -40px;
  }
  @media (min-width: 1400px) {
    margin-left: -70px;
  }
`

const FaqPuffyImg = styled.img`
  z-index: 1;
  width: auto;
  // max-width: 344px;

  @media (min-width: 768px) {
    max-width: 376px;
  }
  @media (min-width: 1200px) {
    max-width: 376px;
  }
  @media (min-width: 1400px) {
    max-width: 627px;
  }
`

export default Faq

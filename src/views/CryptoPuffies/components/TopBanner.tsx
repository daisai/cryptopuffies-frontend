import React from 'react'
import styled from 'styled-components'
import { Card } from 'penguinfinance-uikit2'

const TopBanner = () => {
  return (
    <StyledCard>
      <BannerImage src={`${process.env.PUBLIC_URL}/images/cryptopuffies/puffies_banner.png`} alt="v2 farm banner" />
    </StyledCard>
  )
}

const StyledCard = styled(Card)`
  border-radius: 0px;
  padding: 0 16px;
  background: white;
  @media (min-width: 768px) {
    padding: 0 40px;
  }
  @media (min-width: 1200px) {
    padding: 0;
  }
`

const BannerImage = styled.img`
  width: 100%;
  z-index: -1;
`

export default TopBanner

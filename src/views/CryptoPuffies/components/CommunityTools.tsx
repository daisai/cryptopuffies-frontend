import React from 'react'
import styled from 'styled-components'
import { Card, Flex, Text, Button } from 'penguinfinance-uikit2'

const CommunityTools = () => {
  return (
    <>
      <CommunityToolsHeader>
        <CardContainer flexDirection="column">
          <HeaderText>
            <FirstSentence>{`Community `}</FirstSentence>
            <SecondSentence>{`Tools `}</SecondSentence>
          </HeaderText>
          <DescriptionText>
            {`Here are some helpful tools created by the Bored Ape Yacht Club community. Please note that these are unofficial in nature. Every assignment of an ape's overall value or rarity is inherently subjective. `}
          </DescriptionText>
          <Actions flexWrap='wrap'>
            <StyledButton type="primary">NFTEXP.IO</StyledButton>
            <StyledButton type="secondary">RARITY.TOOLS</StyledButton>
          </Actions>
        </CardContainer>
        <CommunityToolsPuffyImgWrapper>
          <CommunityToolsPuffyImg
            src={`${process.env.PUBLIC_URL}/images/cryptopuffies/community/community_puffy.png`}
            alt="community puffy"
          />
        </CommunityToolsPuffyImgWrapper>
      </CommunityToolsHeader>
    </>
  )
}

// CommunityTools header
const CommunityToolsHeader = styled(Card)`
  position: relative;
  overflow: visible;
  display: flex;
  justify-content: center;
  box-shadow: none;
  border-radius: 0px;
  z-index: 0;
  background: rgba(157, 126, 255, 0.63);
  border-radius: 30px;
  padding: 8px 12px 16px;
  width: 100%;

  @media (min-width: 768px) {
    border-radius: 117px;
    padding: 14px 47px 24px;
  }
`

const CardContainer = styled(Flex)`
  width: 100%;
  background: #ffffff;
  border-radius: 20px;
  padding: 16px 20px;

  @media (min-width: 768px) {
    border-radius: 117px;
    padding: 24px;
  }
`
const HeaderText = styled(Flex)`
  font-size: 52px;
  line-height: 72px;
  white-space: break-spaces;
  justify-content: center;
`
const FirstSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`
const SecondSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`

const DescriptionText = styled(Flex)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;
  margin-top: 8px;
  text-align: center;
  margin-top: 8px;
  padding: 0px;
  color: #050021;
  opacity: 0.8;

  @media (min-width: 768px) {
    padding: 0px 160px;
  }
  @media (min-width: 1200px) {
    padding: 0px 120px;
  }
  @media (min-width: 1400px) {
    padding: 0px 224px;
  }
`

const Actions = styled(Flex)`
  justify-content: center;
  margin-top: 24px;
`
const StyledButton = styled(Button)<{ type: string }>`
  background: ${({ type }) => type === 'primary' && '#9d7eff'};
  background: ${({ type }) => type === 'secondary' && '#ffffff'};
  color: ${({ type }) => type === 'primary' && '#ffffff'};
  color: ${({ type }) => type === 'secondary' && '#9d7eff'};
  border: 1px solid ${({ type }) => type === 'primary' ? '#7eb2ff' : '#9d7eff'};
  box-sizing: border-box;
  border-radius: 32px;
  display: flex;
  align-items: center;
  margin: 0px 15px;
  padding: 15px 40px;
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 17px;
  box-shadow: none;
  width: 182px;
  margin-bottom: 8px;

  @media (min-width: 768px) {
    width: unset;
    margin-bottom: 0;
  }
`

const CommunityToolsPuffyImgWrapper = styled(Flex)`
  position: absolute;
  right: -20px;
  top: -120px;

  @media (min-width: 768px) {
    right: -126px;
  }
  @media (min-width: 1200px) {
    right: -126px;
  }
  @media (min-width: 1400px) {
    right: -156px;
    top: -70px;
  }
`
const CommunityToolsPuffyImg = styled.img`
  z-index: 1;
  width: auto;
  max-width: 165px;

  @media (min-width: 768px) {  
    max-width: 400px;
  }
  @media (min-width: 1200px) {
    max-width: 400px;
  }
  @media (min-width: 1400px) {
    max-width: 450px;
  }
`

export default CommunityTools

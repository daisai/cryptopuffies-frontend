import React from 'react'
import styled from 'styled-components'
import { Card, Text } from 'penguinfinance-uikit2'

const WhatCryptopuffies = () => {
  return (
    <StyledCard>
      <LeftPanel>
        <div>
          <HeaderText>
            <FirstSentence>What are</FirstSentence>
            <SecondSentence>Cryptopuffies?</SecondSentence>
          </HeaderText>
          <DescriptionText>
            <p>
              {`CryptoPuffies are an Avalanche NFT collection of 10,000 randomly generated cute monster avatars. Each
              Puffy is completely unique with its own colours, characteristics, and styles. `}
            </p>
            <p>
              {`The CryptoPuffie collection goes beyond intangible value, as each one comes with utility within the
              @penguin_defi ecosystem. Owners will be able to place their Cryptopuffie NFT’s in an igloo providing them
              with plenty of PEFI & iPEFI rewards. `}
            </p>
          </DescriptionText>
        </div>
      </LeftPanel>
      <RightPanel>
        <CryptoPuffiesGroupImg
          src={`${process.env.PUBLIC_URL}/images/cryptopuffies/what-are-cryptopuffies/cryptopuffies_group.png`}
          alt="love text"
        />
      </RightPanel>
    </StyledCard>
  )
}

const StyledCard = styled(Card)`
  display: flex;
  background: white;
  justify-content: space-between;
  box-shadow: none;
  position: relative;
  overflow: visible;
  border-radius: 0px;
  z-index: 0;
  flex-direction: column;

  @media (min-width: 968px) {
    flex-direction: row;
  }
`

const LeftPanel = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  padding-right: 16px;

  @media (min-width: 968px) {
    width: 50%;
  }
`
const HeaderText = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  @media (min-width: 1200px) {
    flex-direction: column;
  }
  @media (min-width: 1400px) {
    flex-direction: row;
  }
`

const FirstSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: normal;
  font-size: 26px;
  line-height: 42px;
  color: #050021;
  white-space: nowrap;
  margin-right: 8px;

  @media (min-width: 768px) {
    font-size: 52px;
    line-height: 72px;
  }
`
const SecondSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;

  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 52px;
    line-height: 72px;
  }
`

const DescriptionText = styled.div`
  p {
    margin-top: 8px;
    font-family: 'Gotham';
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 27px;
    padding-right: 36px;
    color: #050021;

    @media (min-width: 768px) {
      font-size: 18px;
    }
  }
`

const RightPanel = styled.div`
  width: 100%;
  overflow: hidden;
  margin-top: 20px;

  @media (min-width: 968px) {
    width: 50%;
    margin-top: 0;
  }
`

const CryptoPuffiesGroupImg = styled.img`
  width: 120%;
`

export default WhatCryptopuffies

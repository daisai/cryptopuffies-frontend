import React from 'react'
import styled from 'styled-components'
import { Card, Flex, Text } from 'penguinfinance-uikit2'

const Welcome = () => {
  return (
    <StyledCard>
      <CardContainer flexDirection="column">
        <HeaderText>
          <FirstSentence>{`Welcome to the `}</FirstSentence>
          <SecondSentence>{`Club `}</SecondSentence>
        </HeaderText>
        <DescriptionText>
          {`Dedicated enthusiasts...at vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. `}
        </DescriptionText>
      </CardContainer>
      <WelcomePuffyImgWrapper>
        <WelcomePuffyImg
          src={`${process.env.PUBLIC_URL}/images/cryptopuffies/welcome/welcome_puffy.png`}
          alt="welcome puffy"
        />
      </WelcomePuffyImgWrapper>
    </StyledCard>
  )
}

const StyledCard = styled(Card)`
  position: relative;
  overflow: visible;
  display: flex;
  justify-content: center;
  box-shadow: none;
  border-radius: 0px;
  z-index: 0;
  background: rgba(157, 126, 255, 0.63);
  border-radius: 30px;
  padding: 8px 12px 16px;
  width: 100%;

  @media (min-width: 768px) {
    border-radius: 117px;
    padding: 14px 47px 24px;
  }
`

const CardContainer = styled(Flex)`
  width: 100%;
  background: #ffffff;
  border-radius: 20px;
  padding: 16px 20px;

  @media (min-width: 768px) {
    border-radius: 117px;
    padding: 24px;
  }
`
const HeaderText = styled(Flex)`
  font-size: 52px;
  line-height: 72px;
  white-space: break-spaces;
  justify-content: center;
`
const FirstSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
  @media (min-width: 1400px) {
    font-size: 52px;
  }
`
const SecondSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
  @media (min-width: 1400px) {
    font-size: 52px;
  }
`

const DescriptionText = styled(Flex)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;
  margin-top: 8px;
  text-align: center;
  margin-top: 8px;
  color: #050021;

  @media (min-width: 768px) {
    padding: 0px 48px;
  }
  @media (min-width: 968px) {
    padding: 0px 120px;
  }
  @media (min-width: 1200px) {
    padding: 0px 120px;
  }
  @media (min-width: 1400px) {
    padding: 0px 204px;
  }
`

const WelcomePuffyImgWrapper = styled(Flex)`
  position: absolute;
  top: -140px;
  left: -72px;
  @media (min-width: 768px) {
    left: -200px;
    top: -140px;
  }
  @media (min-width: 968px) {
    left: -160px;
    top: -80px;
  }
  @media (min-width: 1200px) {
    left: -200px;
    top: -100px;
  }
  @media (min-width: 1400px) {
    left: -150px;
    top: -220px;
  }
`
const WelcomePuffyImg = styled.img`
  z-index: 1;
  width: 200px;
  max-width: auto;

  @media (min-width: 768px) {
    width: 396px;
  }
  @media (min-width: 1200px) {
    width: 429px;
    max-width: 429px;
  }
`

export default Welcome

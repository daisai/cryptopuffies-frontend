import React from 'react'
import styled from 'styled-components'
import { Flex, Text, Button } from 'penguinfinance-uikit2'
import SvgIcon from 'components/SvgIcon'
import useWindowSize from 'hooks/useWindowSize'

const collections = [
  { id: 1, name: 'Cute 1', price: '0.689', image: 'collection1.png', bgColor: '#FFE78A' },
  { id: 2, name: 'Cute 1', price: '0.689', image: 'collection2.png', bgColor: '#FF7A84' },
  { id: 3, name: 'Cute 1', price: '0.689', image: 'collection3.png', bgColor: '#8AE083' },
  { id: 4, name: 'Cute 1', price: '0.689', image: 'collection4.png', bgColor: '#FEB17E' },
  { id: 5, name: 'Cute 1', price: '0.689', image: 'collection5.png', bgColor: '#FFC5E3' },
  { id: 6, name: 'Cute 1', price: '0.689', image: 'collection6.png', bgColor: '#8AE083' },
]

const ExploreCollection = () => {
  const windowSize = useWindowSize();

  const colCount = windowSize.width >= 768 ? 4 : 2;

  return (
    <>
      <ExploreCollectionHeader>
        <ExploreCollectionHeaderTitle>
          <FirstSentence>{`Explore `}</FirstSentence>
          <SecondSentence>{`Collection `}</SecondSentence>
        </ExploreCollectionHeaderTitle>
        <ExploreCollectionHeaderDescription>
          {`By joining us you’re not investing into a project, you’re helping build the universe `}
        </ExploreCollectionHeaderDescription>
        <StatisticInfo>
          <StatisticsCard>
            <Flex>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/user_icon.svg`}
                width="20px"
                height="20px"
              />
              <Value>5K</Value>
            </Flex>
            <Field>owners</Field>
          </StatisticsCard>
          <StatisticsCard>
            <Flex>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/avax_icon.svg`}
                width="20px"
                height="20px"
              />
              <Value>0.689</Value>
            </Flex>
            <Field>floor price</Field>
          </StatisticsCard>
          <StatisticsCard>
            <Flex>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/avax_icon.svg`}
                width="20px"
                height="20px"
              />
              <Value>1.8K</Value>
            </Flex>
            <Field>traded</Field>
          </StatisticsCard>
        </StatisticInfo>
        <ExplorePuffyImgWrapper>
          <ExplorePuffyImg
            src={`${process.env.PUBLIC_URL}/images/cryptopuffies/explore/explore_puffy.png`}
            alt="explore puffy"
          />
        </ExplorePuffyImgWrapper>
      </ExploreCollectionHeader>
      <CollectionListContainer>
        {(windowSize.width > 1400 ? collections : collections.filter((collection, index) => index < colCount)).map((collection) => (
          <CollectionCard key={collection.id}>
            <CollectionImageWrapper color={collection.bgColor}>
              <CollectionImage
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/${collection.image}`}
                alt="collection image"
              />
              <VoteIconWrapper>
                <SvgIcon
                  src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/heart_icon.svg`}
                  width="20px"
                  height="20px"
                />
              </VoteIconWrapper>
            </CollectionImageWrapper>
            <CollectionInfo>
              <CollectionName>{collection.name}</CollectionName>
              <CollectionPrice>
                <span>
                  <SvgIcon
                    src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/avax_icon.svg`}
                    width="20px"
                    height="20px"
                  />
                </span>
                {collection.price}
              </CollectionPrice>
            </CollectionInfo>
          </CollectionCard>
        ))}
      </CollectionListContainer>
      <Actions>
        <BrowseButton>
          BROWSE ALL
          <SvgIcon
            src={`${process.env.PUBLIC_URL}/images/cryptopuffies/collection/grip_group_icon.svg`}
            width="24px"
            height="24px"
          />
        </BrowseButton>
      </Actions>
    </>
  )
}

// header
const ExploreCollectionHeader = styled.div`
  margin: auto;
  text-align: center;
  position: relative;
  min-height: 320px;
  margin-bottom: 32px;

  @media (min-width: 768px) {
    min-height: 468px;
    margin-bottom: 0;
  }
`
const ExploreCollectionHeaderTitle = styled(Flex)`
  white-space: break-spaces;
  flex-wrap: wrap;
  justify-content: center;
`
const FirstSentence = styled.span`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`
const SecondSentence = styled.span`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`
const ExploreCollectionHeaderDescription = styled.div`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;
  max-width: 580px;
  margin: auto;
  margin-top: 8px;
`

// statistics info
const StatisticInfo = styled(Flex)`
  justify-content: center;
  margin-top: 16px;
`
const StatisticsCard = styled.div`
  padding: 16px 24px 14px;
  background: #ffffff;
  box-shadow: 4px 4px 15px rgba(166, 129, 255, 0.08);
  border-radius: 8px;
  margin: 0px 8px;

  @media (min-width: 768px) {
  }
  @media (min-width: 1200px) {
    padding: 16px 24px 14px;
  }
  @media (min-width: 1400px) {
    padding: 16px 32px 14px;
    margin: 0 16px;
  }
`
const Value = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  color: #050021;
  margin-left: 7px;

  @media (min-width: 768px) {
    font-size: 24px;
  }
`
const Field = styled(Text)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 27px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 18px;
  }
`

// list
const CollectionListContainer = styled.div`
  display: grid;
  grid-gap: 30px;
  grid-template-columns: repeat(2, 1fr);
  padding: 16px 0;

  @media (min-width: 768px) {
    grid-gap: 24px;
    grid-template-columns: repeat(4, 1fr);
    padding: 24px 0;
  }
  @media (min-width: 1200px) {
    grid-template-columns: repeat(4, 1fr);
  }
  @media (min-width: 1400px) {
    grid-template-columns: repeat(6, 1fr);
  }
`
// background image
const ExplorePuffyImgWrapper = styled(Flex)`
  position: absolute;
  left: 0;
  right: 0;
  top: 200px;

  @media (min-width: 640px) {
    left: 0;
    right: 0;
    top: 140px;
  }

  @media (min-width: 968px) {
    left: 0;
    right: 0;
    top: 60px;
  }
  @media (min-width: 1400px) {
    left: -100px;
    right: -100px;
    top: -50px;
  }
`
const ExplorePuffyImg = styled.img`
  z-index: -1;
  width: auto;
`

const CollectionCard = styled.div`
  width: 100%;
`
const CollectionImageWrapper = styled(Flex)<{ color: string }>`
  position: relative;
  background: ${({ color }) => color};
  border-radius: 16px;
  // max-width: 180px;
  // height: 180px;
  width: 100%;
  height: 100%;
  max-height: 180px;
  align-items: center;
  padding: 24px;

  @media (min-width: 968px) {
    max-height: 240px;
  }

  @media (min-width: 1200px) {
    max-height: 180px;
  }
  @media (min-width: 1400px) {
    max-height: 180px;
  }
`
const CollectionImage = styled.img`
  height: 100px;
  margin-left: auto;
  margin-right: auto;

  @media (min-width: 968px) {
    height: 160px;
  }

  @media (min-width: 1200px) {
    height: unset;
  }
`
const VoteIconWrapper = styled(Flex)`
  position: absolute;
  top: 8px;
  right: 8px;
  height: 40px;
  width: 40px;
  align-items: center;
  justify-content: center;
  background: rgba(255, 255, 255, 0.4);
  backdrop-filter: blur(37px);
  border-radius: 50%;
`
const CollectionInfo = styled(Flex)`
  text-align: center;
  padding: 8px;
  justify-content: space-between;
  // max-width: 180px;
`
const CollectionName = styled(Text)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 27px;
  color: #050021;

  @media (min-width: 968px) {
    font-size: 18px;
    line-height: 27px;
  }
`
const CollectionPrice = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 24px;
  color: #050021;
  // margin: auto;
  display: flex;
  margin-left: 4px;

  @media (min-width: 968px) {
    font-size: 24px;
    line-height: 24px;
  }
`

// actions
const Actions = styled(Flex)`
  justify-content: center;
  margin-top: 40px;

  @media (min-width: 978px) {
    margin-top: 40px;
  }
  @media (min-width: 1200px) {
    margin-top: 20px;
  }
  @media (min-width: 1400px) {
    margin-top: 0;
  }
`
const BrowseButton = styled(Button)`
  font-family: 'Gotham-bold';
  padding: 12px 30px;
  background: #9d7eff !important;
  box-shadow: none;
  align-items: center;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 27px;
  text-transform: uppercase;
  border-radius: 32px;

  > div {
    align-items: center;
    display: flex;
    margin-left: 10px;
  }
`

export default ExploreCollection

import React from 'react'
import styled from 'styled-components'
import Slider from "react-slick";
import { Card, Flex, Text } from 'penguinfinance-uikit2'
import SvgIcon from 'components/SvgIcon'
import useWindowSize from 'hooks/useWindowSize'

interface RoadmapStepProps {
  step: number
  title: string
  description: string
  position: string
  color: string
  mainIcon?: string
  wrapperIcon?: string
  mobileIcon?: string
  mobileWrapperIcon?: string
  wrapperIconSize?: number
}

const roadmapSteps = [
  {
    step: 1,
    title: 'Launch Roadmap',
    description:
      'Egobirade pretäling pneumatropi. Polinetik netosm spesahösk anuktig visa. Hopp lavigon fastän teletres. Treligt exon. Rektigt ultrade niren. Laling diledes dire. Poska prel, satavis och curlingförälder, predugt. Vägg-tv filterbubbla. Rärar tempoaktiv, belingar och suvis, när mikrovosade. Mikrobende krodade, planar. Stenov dagshandlare, neledes. Jösk didat i reledes. ',
    position: 'left',
    color: '#8AE083',
    mainIcon: 'step1_main_icon.svg',
    wrapperIcon: 'step1_wrapper_icon.svg',
    mobileIcon: 'step1_main_icon_mobile.svg',
    mobileWrapperIcon: 'step1_wrapper_icon_mobile.svg',
    wrapperIconSize: 32,
  },
  {
    step: 2,
    title: 'Physical Part',
    description:
      'Egobirade pretäling pneumatropi. Polinetik netosm spesahösk anuktig visa. Hopp lavigon fastän teletres. Treligt exon. Rektigt ultrade niren. Laling diledes dire. Poska prel, satavis och curlingförälder, predugt. Vägg-tv filterbubbla. Rärar tempoaktiv, belingar och suvis, när mikrovosade. Mikrobende krodade, planar. Stenov dagshandlare, neledes. Jösk didat i reledes. ',
    position: 'right',
    color: '#feb17e',
    mainIcon: 'step2_main_icon.svg',
    wrapperIcon: 'step2_wrapper_icon.svg',
    mobileIcon: 'step2_main_icon_mobile.svg',
    mobileWrapperIcon: 'step2_wrapper_icon_mobile.svg',
    wrapperIconSize: 40,
  },
  {
    step: 3,
    title: 'Website',
    description:
      'Egobirade pretäling pneumatropi. Polinetik netosm spesahösk anuktig visa. Hopp lavigon fastän teletres. Treligt exon. Rektigt ultrade niren. Laling diledes dire. Poska prel, satavis och curlingförälder, predugt. ',
    position: 'left',
    color: '#FFC5E3',
    mainIcon: 'step3_main_icon.svg',
    wrapperIcon: 'step3_wrapper_icon.svg',
    mobileIcon: 'step3_main_icon_mobile.svg',
    mobileWrapperIcon: 'step3_wrapper_icon_mobile.svg',
    wrapperIconSize: 60,
  },
  {
    step: 4,
    title: 'Mint',
    description:
      'Rektigt ultrade niren. Laling diledes dire. Poska prel, satavis och curlingförälder, predugt. Vägg-tv filterbubbla. Rärar tempoaktiv, belingar och suvis, när mikrovosade. Mikrobende krodade, planar. Stenov dagshandlare, neledes. Jösk didat i reledes. Egobirade pretäling pneumatropi. Polinetik netosm spesahösk anuktig visa. Hopp lavigon fastän teletres. Treligt exon.',
    position: 'right',
    color: '#D1ABEF',
    mainIcon: 'step4_main_icon.svg',
    wrapperIcon: 'step4_wrapper_icon.svg',
    mobileIcon: 'step4_main_icon_mobile.svg',
    mobileWrapperIcon: 'step4_wrapper_icon_mobile.svg',
    wrapperIconSize: 80,
  },
]

const RoadmapStep = ({ data }: { data: RoadmapStepProps }) => {
  return (
    <RoadmapStepWrapper>
      {data.step % 2 === 1 ? (
        <>
          <StepCard borderColor={data.color} step={data.step}>
            <StepTitle>{data.title}</StepTitle>
            <StepDescription>{data.description}</StepDescription>
          </StepCard>
          <StepGraph>
            <Dot color={data.color} stepPosition={data.position} />
            <Line color={data.color} />
            <IconContainer stepPosition={data.position}>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/${data.mainIcon}`}
                width={`${data.wrapperIconSize}px`}
                height={`${data.wrapperIconSize}px`}
              />
            </IconContainer>
            <IconWrapperContainer stepPosition={data.position}>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/${data.wrapperIcon}`}
                width={`${data.wrapperIconSize}px`}
                height={`${data.wrapperIconSize}px`}
              />
            </IconWrapperContainer>
          </StepGraph>
          <StepPercent color={data.color} step={data.step}>{`${25 * data.step}%`}</StepPercent>
        </>
      ) : (
        <>
          <StepPercent color={data.color} step={data.step}>{`${25 * data.step}%`}</StepPercent>
          <StepGraph>
            <IconContainer stepPosition={data.position}>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/${data.mainIcon}`}
                width={`${data.wrapperIconSize}px`}
                height={`${data.wrapperIconSize}px`}
              />
            </IconContainer>
            <IconWrapperContainer stepPosition={data.position}>
              <SvgIcon
                src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/${data.wrapperIcon}`}
                width={`${data.wrapperIconSize}px`}
                height={`${data.wrapperIconSize}px`}
              />
            </IconWrapperContainer>
            <Line color={data.color} />
            <Dot color={data.color} stepPosition={data.position} />
          </StepGraph>
          <StepCard borderColor={data.color} step={data.step}>
            <StepTitle>{data.title}</StepTitle>
            <StepDescription>{data.description}</StepDescription>
          </StepCard>
        </>
      )}
    </RoadmapStepWrapper>
  )
}

const MobileRoadmapStep = ({ data }: { data: RoadmapStepProps }) => {
  return (
    <div>
      <CardWrapper>
        <StepPercent color={data.color} step={data.step}>{`${25 * data.step}%`}</StepPercent>
        <StepDivider />
        <StepGraph>
          <IconContainer stepPosition={data.position}>
            <SvgIcon
              src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/${data.mobileIcon}`}
              width={`${data.wrapperIconSize}px`}
              height={`${data.wrapperIconSize}px`}
            />
          </IconContainer>
          <IconWrapperContainer stepPosition={data.position}>
            <SvgIcon
              src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/${data.mobileWrapperIcon}`}
              width={`${data.wrapperIconSize}px`}
              height={`${data.wrapperIconSize}px`}
            />
          </IconWrapperContainer>
          <Line color={data.color} />
          <Dot color={data.color} stepPosition={data.position} />
        </StepGraph>
        <StepCard borderColor={data.color} step={data.step}>
          <StepTitle>{data.title}</StepTitle>
          <StepDescription>{data.description}</StepDescription>
        </StepCard>
      </CardWrapper>
    </div>
  )
}

const Roadmap = () => {
  const windowSize = useWindowSize();

  const settings = {
    // className: "center",
    // centerMode: true,
    infinite: true,
    // centerPadding: "60px",
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    nextArrow: (
      <NextArrow>
        <SvgIcon src={`${process.env.PUBLIC_URL}/images/cryptopuffies/team/arrow-left.svg`} width='20px' height='20px' />
      </NextArrow>
    )
  };

  return (
    <>
      <RoadmapHeader>
        <CardContainer flexDirection="column">
          <HeaderText>
            <FirstSentence>{`Our `}</FirstSentence>
            <SecondSentence>{`Roadmap `}</SecondSentence>
          </HeaderText>
          <DescriptionText>
            {`The CryptoPuffies team works hard on bringing to life the cutest NFT collection yet. The further we go, the better we’re gonna be. `}
          </DescriptionText>
        </CardContainer>
        <RoadmapPuffyImgWrapper>
          <RoadmapPuffyImg
            src={`${process.env.PUBLIC_URL}/images/cryptopuffies/roadmap/roadmap_puffy.png`}
            alt="roadmap puffy"
          />
        </RoadmapPuffyImgWrapper>
      </RoadmapHeader>
      <RoadmapContent>
        {windowSize.width < 768 ? 
          <SliderContainer flexDirection='column' mt='40px'>
            <Slider {...settings}>
              {roadmapSteps.map(row => {
                return (
                  <MobileRoadmapStep key={row.step} data={row} />
                )
              })}
            </Slider>
          </SliderContainer>
        : 
          <>
            {roadmapSteps.map((row) => {
              return <RoadmapStep key={row.step} data={row} />
            })}
          </>
        }
        {windowSize.width >= 768 && 
          <RoadmapContentDivider />
        }
      </RoadmapContent>
    </>
  )
}

const CardWrapper = styled.div`
  position: relative;
`;

const NextArrow = styled.div`
  div {
    position: absolute;
  }
`;

const SliderContainer = styled(Flex)`
  .slick-arrow {
    position: absolute;
    left: 20px;
    top: -8px;
  }
`;

// roadmap header
const RoadmapHeader = styled(Card)`
  position: relative;
  overflow: visible;
  display: flex;
  justify-content: center;
  box-shadow: none;
  border-radius: 0px;
  z-index: 0;
  background: rgba(157, 126, 255, 0.63);
  border-radius: 30px;
  padding: 8px 12px 16px;
  width: 100%;

  @media (min-width: 768px) {
    border-radius: 117px;
    padding: 14px 47px 24px;
  }
`

const CardContainer = styled(Flex)`
  width: 100%;
  background: #ffffff;
  border-radius: 20px;
  padding: 16px 20px;

  @media (min-width: 768px) {
    border-radius: 117px;
    padding: 24px;
  }
`
const HeaderText = styled(Flex)`
  font-size: 52px;
  line-height: 72px;
  white-space: break-spaces;
  justify-content: center;
`
const FirstSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  color: #050021;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`
const SecondSentence = styled(Text)`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 26px;
  line-height: 42px;
  background: linear-gradient(to right, #9d7eff 0%, #ffc5e3 100%);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;

  @media (min-width: 768px) {
    font-size: 26px;
  }
  @media (min-width: 968px) {
    font-size: 42px;
    line-height: 72px;
  }
  @media (min-width: 1200px) {
    font-size: 52px;
  }
`

const DescriptionText = styled(Flex)`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;
  margin-top: 8px;
  text-align: center;
  margin-top: 8px;
  padding: 0px;
  color: #050021;
  opacity: 0.8;

  @media (min-width: 768px) {
    padding: 0px 120px;
  }
  @media (min-width: 1200px) {
    padding: 0px 160px;
  }
  @media (min-width: 1400px) {
    padding: 0px 224px;
  }
`

const RoadmapPuffyImgWrapper = styled(Flex)`
  position: absolute;
  right: -90px;
  top: -100px;

  @media (min-width: 768px) {
    right: -200px;
    top: -80px;
  }
  @media (min-width: 1200px) {
    right: -180px;
    top: -80px;
  }
  @media (min-width: 1400px) {
    right: -180px;
    top: -80px;
  }
`

const RoadmapPuffyImg = styled.img`
  z-index: 1;
  width: 200px;
  max-width: auto;

  @media (min-width: 768px) {
    width: 410px;
  }
  @media (min-width: 1200px) {
    width: 410px;
  }
  @media (min-width: 1400px) {
    width: 554px;
  }
`

// roadmap content
const RoadmapContent = styled.div`
  margin-top: 80px;
  position: relative;
`

// roadmap content divider
const RoadmapContentDivider = styled.div`
  position: absolute;
  top: 0px;
  bottom: 0px;
  left: 50%;
  height: 100%;
  width: 2px;
  background: radial-gradient(53.32% 53.32% at 50% 50%, #d1e3ff 0%, #d1e3ff 0.01%, rgba(209, 227, 255, 0) 100%);
`

const StepDivider = styled.div`
  position: absolute;
  top: 56px;
  height: 2px;
  width: 100%;
  background: radial-gradient(53.32% 53.32% at 50% 50%, #d1e3ff 0%, #d1e3ff 0.01%, rgba(209, 227, 255, 0) 100%);
`;

const RoadmapStepWrapper = styled(Flex)``
const StepCard = styled.div<{ borderColor: string; step: number }>`
  padding: 20px;
  // max-width: 50%;
  border-radius: 22px;
  border: ${({ borderColor }) => `1px solid ${borderColor}`};
  /* margin-bottom: -40px; */
  margin-bottom: ${({ step }) => step !== 4 && '-40px'};
  margin-top: 100px;

  @media (min-width: 768px) {
    max-width: 50%;
    padding: 38px;
    margin-top: 0;
  }
`
const StepTitle = styled.div`
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 24px;
  color: #050021;
  opacity: 0.8;
`
const StepDescription = styled.div`
  font-family: 'Gotham';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;
  color: #050021;
  margin-top: 16px;
`

const StepGraph = styled(Flex)`
  position: relative;
  align-items: center;
`
const Dot = styled(Flex)<{ color: string; stepPosition: string }>`
  position: absolute;
  height: 10px;
  width: 10px;
  background: ${({ color }) => color};
  border-radius: 50%;
  margin: ${({ stepPosition }) => (stepPosition === 'left' ? '-5px' : '-5px')};
  right: ${({ stepPosition }) => stepPosition === 'right' && '0px'};
  left: ${({ stepPosition }) => stepPosition === 'left' && '0px'};

  position: absolute;
  left: 50%;
  top: 100px;

  @media (min-width: 768px) {
    position: initial;
    left: unset;
    top: unset;
  }
`

const Line = styled(Flex)<{ color: string }>`
  width: 1px;
  height: 30px;
  background: ${({ color }) => color};
  position: absolute;
  left: 50%;
  top: 70px;

  @media (min-width: 768px) {
    position: initial;
    left: unset;
    top: unset;
    height: 1px;
    width: 86px;
  }
`

const IconContainer = styled.div<{ stepPosition?: string }>`
  position: absolute;
  z-index: 2;
  left: 50%;
  transform: translate(-50%, -50%);
  top: 56px;

  @media (min-width: 768px) {
    top: unset;
    left: unset;
    transform: unset;
    right: ${({ stepPosition }) => stepPosition === 'left' && '0px'};
    left: ${({ stepPosition }) => stepPosition === 'right' && '0px'};
    transform: ${({ stepPosition }) => stepPosition === 'left' && 'translateX(50%);'};
    transform: ${({ stepPosition }) => stepPosition === 'right' && 'translateX(-50%);'};
  }
`

const IconWrapperContainer = styled.div<{ stepPosition?: string }>`
  position: absolute;
  left: 50%;
  transform: translate(-50%, -50%);
  top: 56px;
  z-index: 1;

  @media (min-width: 768px) {
    top: unset;
    left: unset;
    transform: unset;
    right: ${({ stepPosition }) => stepPosition === 'left' && '0px'};
    left: ${({ stepPosition }) => stepPosition === 'right' && '0px'};
    transform: ${({ stepPosition }) => stepPosition === 'left' && 'translateX(50%);'};
    transform: ${({ stepPosition }) => stepPosition === 'right' && 'translateX(-50%);'};
  }
`

const StepPercent = styled(Flex)<{ color: string; step: number }>`
  align-items: center;
  // justify-content: ${({ step }) => step % 2 === 0 && 'end'};
  // padding-left: ${({ step }) => step % 2 === 1 && '63px'};
  // padding-right: ${({ step }) => step % 2 === 0 && '63px'};
  font-family: 'Gotham-bold';
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 24px;
  color: ${({ color }) => color};
  // min-width: 50%;

  position: absolute;
  left: 50%;
  transform: translate(-50%, 0);
  z-index: 1;

  @media (min-width: 768px) {
    position: unset;
    left: unset;
    transform: unset;
    z-index: 0;

    
    justify-content: ${({ step }) => step % 2 === 0 && 'end'};
    padding-left: ${({ step }) => step % 2 === 1 && '63px'};
    padding-right: ${({ step }) => step % 2 === 0 && '63px'};
    min-width: 50%;
  }
`

export default Roadmap
